// Adding / Removing rows
function cloneRow(){
	let newLine = $(this).parent().clone();
	$(this).hide();
	console.log('new line: ', newLine);
	$('#newRow').append(newLine);
	$('button[name=remove]').click(removeRow);
	$('button[name=add]').last().click(cloneRow);
}
$('button[name=add]').click(cloneRow);
function removeRow(){
	$(this).parent().fadeOut('200', function(){
		$(this).remove();
		console.log('this: ', $(this));
	});
	if ($(this).parent().is(':last-child')){
		var addBtn = $('#newRow').find('button[name=add]').eq(-2);
		console.log('last addBtn', addBtn);
		$(addBtn).show();
	} else {
		// var addBtn = $('#newRow').find('button[name=add]').eq(-1);
		console.log('not last addBtn', addBtn);
	}
}
$('button[name=remove]').click(removeRow);
